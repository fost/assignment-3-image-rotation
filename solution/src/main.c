#include "file_worker/file_worker.h"
#include "bmp/bmp_convert.h"
#include "image/image.h"
#include "transformations/transfrormations.h"
#include "util/status_handler.h"

void check_return_status(enum status stat, struct image *img) {
    if (stat != OK) {
        if (img != NULL) image_destroy(*img);
        print_status(stat);
        exit(1);
    }
}

int main (int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "not enough args\n");
        return 0;
    }
    const char *const input_file_name = argv[1];
    const char *const output_file_name = argv[2];

    struct optional_file maybe_file  = file_read_open(input_file_name);
    check_return_status(maybe_file.file_status, NULL);

    struct image img;
    check_return_status(from_bmp(maybe_file.file, &img), &img);

    struct optional_image new_img = rotate(90, img);
    check_return_status(new_img.image_status, &img);

    struct optional_file maybe_file_out  = file_write_open(output_file_name);
    check_return_status(maybe_file_out.file_status, &new_img.img);

    check_return_status(to_bmp(maybe_file_out.file, &new_img.img), &new_img.img);

    image_destroy(new_img.img);
}
