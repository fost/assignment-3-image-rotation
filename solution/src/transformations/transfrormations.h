#pragma once

#include "../image/image.h"
#include "../util/status.h"

struct optional_image {
    enum status image_status;
    struct image img;
};

struct optional_image rotate(size_t degree, struct image img);
