#include "transfrormations.h"

struct optional_image rotate_image_90(const struct image input);

struct optional_image rotate(size_t degree, struct image img) {
    size_t normalized_degree = degree % 360;
    if (normalized_degree == 90) {
        return rotate_image_90(img);
    }
    return (struct optional_image) { .image_status = NOT_IMPLEMENTED, .img = { .data = NULL } };
}

struct optional_image rotate_image_90(const struct image input) {
    size_t newWidth = input.height;
    size_t newHeight = input.width;

    struct image output = image_create(newWidth, newHeight);
    if (output.data == NULL)
        return (struct optional_image) { .image_status = MALLOC_ERROR, .img = output };

    for (size_t x = 0; x < input.height; x++) {
        for (size_t y = 0; y < input.width; y++) {
            size_t pixel_height = newWidth - 1 - x;
            output.data[ newWidth * y + x] = input.data[ input.width * pixel_height + y ];
        }
    }

    image_destroy(input);
    return (struct optional_image) { .image_status = OK, .img = output };
}
