#pragma once

#include <stdio.h>

#include "../util/status.h"

struct optional_file {
    enum status file_status;
    FILE *file;
};

struct optional_file file_read_open(const char *filename);
struct optional_file file_write_open(const char *filename);
enum status close_file(FILE *file);
