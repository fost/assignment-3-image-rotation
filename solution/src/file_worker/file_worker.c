#include "file_worker.h"

struct optional_file file_read_open(const char *filename) {
    FILE *file;
    if ((file = fopen(filename, "r")) == NULL) {
        return (struct optional_file) { .file_status = OPEN_TO_READ_ERROR};
    }
    return (struct optional_file) { .file_status = OK, .file = file};
}

struct optional_file file_write_open(const char *filename) {
    FILE *file;
    if ((file = fopen(filename, "w")) == NULL) {
        return (struct optional_file) { .file_status = OPEN_TO_WRITE_ERROR};
    }
    return (struct optional_file) { .file_status = OK, .file = file};
}

enum status close_file(FILE *file) {
    if (fclose(file) == EOF) {
        return CLOSE_ERROR;
    }
    return OK;
}
