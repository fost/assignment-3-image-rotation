#include "image.h"

struct image image_create(size_t const a_width, size_t const a_height) {
    return (struct image) {
        .width = a_width, 
        .height = a_height, 
        .data= (struct pixel *)malloc(a_width * a_height * sizeof(struct pixel))};
}

void image_destroy(struct image an_image) {
    free(an_image.data);
}
