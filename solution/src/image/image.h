#pragma once

#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>

struct pixel {
  uint8_t components[3]; // b g r
};

struct image {
  size_t width, height;
  struct pixel* data;
};

struct image image_create( size_t width, size_t height );
void image_destroy( struct image image );
struct image rotate_image(const struct image *input);
