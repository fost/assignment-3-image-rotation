#include "padding.h"

uint8_t calculate_padding(size_t img_width) {
    size_t pixel_row_size = img_width * sizeof(struct pixel);
    return ( 4 -  pixel_row_size % 4) % 4;
}
