#pragma once

#include <stdint.h>
#include <stdio.h>

#include "../image/image.h"
#include "../util/status.h"

enum status from_bmp(FILE *in, struct image *img);
enum status to_bmp(FILE *out, struct image const *img);
