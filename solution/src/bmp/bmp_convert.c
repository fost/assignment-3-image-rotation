#include "bmp_convert.h"
#include "bmp_header.h"
#include "padding.h"

#include <string.h>

enum status read_header(FILE *f, struct bmp_header *header);

enum status create_bmp_header(struct image const *img, struct bmp_header *header);

struct optional_header {
    enum status status;
    struct bmp_header bmp_header;
};

enum status from_bmp(FILE *const in, struct image *const img) {
    struct optional_header maybe_bmp_header;
    maybe_bmp_header.status = read_header(in, &maybe_bmp_header.bmp_header);

    if (maybe_bmp_header.status == PARSE_ERROR) return PARSE_ERROR;

    const size_t src_width = maybe_bmp_header.bmp_header.biWidth;
    const size_t src_height = maybe_bmp_header.bmp_header.biHeight;

    img[0] = image_create(src_width, src_height);

    const uint8_t padding_size = calculate_padding(src_width);

    for (uint64_t i = 0; i < src_height; i++) {
        void *start_index = img->data + img->width * i;

        fread(start_index, sizeof(struct pixel), src_width, in);
        fseek(in, padding_size, SEEK_CUR);
    }

    return OK;
}

enum status to_bmp(FILE *const out, struct image const *const img) {

    struct bmp_header header = {0};
    create_bmp_header(img, &header);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    fseek(out, header.bOffBits, SEEK_SET);

    const size_t padding = calculate_padding(img->width);

    // Allocate space for padding, filled with garbage values
    uint8_t *line_padding = (uint8_t *)malloc(padding);

    if (!line_padding) return CONVERT_ERROR;

    // Initialize line_padding with zeros
    for (size_t i = 0; i < padding; ++i) {
        line_padding[i] = 0;
    }

    // Write image data to disk, adding padding at the end of each row
    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; ++i) {
            fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, out);
            fwrite(line_padding, padding, 1, out);
        }
    }
    
    free(line_padding);

    return OK;
}

