#include "status_handler.h"

static const char *const error_descriptions[] = {
        [OK] = "INFO: Program executed successful!",
        [OPEN_TO_WRITE_ERROR] = "ERROR: Unable to open bmp file to write!",
        [OPEN_TO_READ_ERROR] = "ERROR: Unable to open bmp file to read!",
        [WRITE_ERROR] = "ERROR: Unable to write to bmp file!",
        [CLOSE_ERROR] = "ERROR: Unable to close bmp file!",
        [PARSE_ERROR] = "ERROR: Unable to read from bmp file!",
        [CONVERT_ERROR] = "ERROR: Unable to write to bmp file!",
        [MALLOC_ERROR] = "ERROR: Couldn't allocate enough memory for image",
        [NOT_IMPLEMENTED] = "ERROR: The functionality is not implemented yet!"
};

void print_status( enum status status ) {
  printf("-->%s\n", error_descriptions[status]);
}

